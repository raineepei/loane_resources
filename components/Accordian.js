
import React, {Component} from 'react';
import { View, TouchableOpacity, Text, StyleSheet, LayoutAnimation, Platform, UIManager} from "react-native";
import { Colors } from './Colors';
import Icon from "react-native-vector-icons/MaterialIcons";

export default class Accordian extends Component{

    constructor(props) {
        super(props);
        this.state = { 
          data: props.data,
          //main expansion
          expanded : false,
          //expansion of first tab
          subexpanded: false,
        }

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }
  
  render() {

    return (
       <View>
            <TouchableOpacity ref={this.accordian} style={styles.row} onPress={()=>this.toggleExpand()}>
                <Text style={[styles.title, styles.font]}>{this.props.title}</Text>
                <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color={Colors.DARKGRAY} />
            </TouchableOpacity>

            <View style={styles.parentHr}/>
            {
                this.state.expanded &&
                <View style={styles.child}>
                    <TouchableOpacity ref={this.accordian} style={styles.row} onPress={()=>this.toggleSubExpand()}>
                        <Text style={[styles.subtitle, styles.font]}>{this.props.subtitle}</Text>
                        <Icon name={this.state.subexpanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color={Colors.DARKGRAY} />
                    </TouchableOpacity>
                </View>
            }

            <View style={styles.child}/>
            {
                this.state.subexpanded &&
                <View style={styles.childOne}>
                    <Text>{this.props.data}</Text>    
                </View>
            }

       </View>
    )
  }

  toggleExpand=()=>{
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded : !this.state.expanded})
  }

  toggleSubExpand=()=>{
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({subexpanded : !this.state.subexpanded})
  }

}

const styles = StyleSheet.create({
    title:{
        fontSize: 20,
        fontWeight:'bold',
        color: Colors.DARKGRAY,
    },
    subtitle:{
        fontSize: 17,
        fontWeight:'normal',
        color: Colors.DARKGRAY,
    },
    row:{
        flexDirection: 'row',
        justifyContent:'space-between',
        height:58,
        paddingLeft:25,
        paddingRight:18,
        alignItems:'center',
        backgroundColor: Colors.CGRAY,
    },
    parentHr:{
        height:1,
        color: Colors.WHITE,
        width:'100%'
    },
    child:{
        backgroundColor: Colors.CGRAY,
        paddingLeft:2,
        padding:3,
    },
    childOne:{
        backgroundColor: Colors.CGRAY,
        paddingLeft:27,
        padding:16,
    }
    
});

