import React, {Component} from 'react';
import { StyleSheet, View} from 'react-native';

export default class App extends Component {

    constructor(props) {
      super(props);
      this.state = {
        menu :[
          { 
            title: 'FAQ', 
            subtitle: 'How do I close my account?',
            subtitle1: 'hi',
            data: 'To close your account, go to the settings page and click on the “Delete Account” button. Just follow the prompts, and you will be on your way to deleting your account.',
          },
          { 
            title: 'LINKS',
            subtitle: 'food',
            data: 'hi'
          },
          { 
           title: 'ABOUT',
           subtitle: 'yoyo',
           data: 'hello'
          },
        ]
       }
    }
  
    render() {
      return (
        <View style={styles.container}>
          { this.renderAccordians() }
        </View>
      );
    }
  
    renderAccordians=()=> {
      const items = [];
      for (item of this.state.menu) {
          items.push(
              <Accordian 
                  title = {item.title}
                  subtitle = {item.subtitle}
                  data = {item.data}
              />
          );
      }
      return items;
  }
  }
  
  const styles = StyleSheet.create({
    container: {
     flex:1,
     paddingTop:100,
     backgroundColor:Colors.PRIMARY,
     
    }
  });