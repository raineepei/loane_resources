import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Button, View, Text} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

function ResourcesPage() {
  return (
    <View>
      <Resources />
    </View>
  );
}

function FaqPage() {
  return (
    <View>
      <Text>Details Screen</Text>
    </View>
  );
}

const Stack = createStackNavigator();

function App() {

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Resources" component={ResourcesPage} />
        <Stack.Screen name="Faq" component={FaqPage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

/*
<Button
title="Go to Details"
onPress={() => navigation.navigate('Details')}
/>
*/